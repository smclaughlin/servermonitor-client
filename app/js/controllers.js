'use strict';


/* Controllers */
var appController = angular.module('myApp.controllers', ['faye']);

appController.factory('Faye', [
  '$faye', function($faye) {
    return $faye("http://107.170.112.187:4000/faye");
  }
]);

appController.controller('MyCtrl1', ['$scope', 'Faye', function($scope, Faye) {
  //Add a count to ensure scope gets digested when consecutive usage values are the same
  var count = 0;

  Faye.subscribe("/test", function(msg) {
  		count++;
  		$scope.usage = {usage : msg.usage};
  });
}]);
