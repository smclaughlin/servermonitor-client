angular.module('myApp')
	.directive('lineChart',[ function(){
		return {
			restrict : 'E',
			scope : {
				points : '=points'
			},
			link : function(scope, element){
				var latestVal = 0;
				
				var n = 243,
				    duration = 750,
				    now = new Date(Date.now() - duration),
				    count = 0,
				    data = d3.range(n).map(function() { return 0; });

				var margin = {top: 6, right: 0, bottom: 20, left: 40},
				    width = 960 - margin.right,
				    height = 300 - margin.top - margin.bottom;

				var x = d3.time.scale()
				    .domain([now - (n - 2) * duration, now - duration])
				    .range([0, width]);

				var y = d3.scale.linear()
				    .range([height, 0])
				    .domain([0, 100]);

				var yAxis = d3.svg.axis()
						    .scale(y)
						    .orient("left");

				var line = d3.svg.line()
				    .interpolate("basis")
				    .x(function(d, i) { return x(now - (n - 1 - i) * duration); })
				    .y(function(d, i) { return y(d); });

				var svg = d3.select(element[0]).append("p").append("svg")
				    .attr("width", width + margin.left + margin.right)
				    .attr("height", height + margin.top + margin.bottom)
				    .style("margin-left", -margin.left + "px")
				  .append("g")
				    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

				svg.append("defs").append("clipPath")
				    .attr("id", "clip")
				  .append("rect")
				    .attr("width", width)
				    .attr("height", height);

				var axis = svg.append("g")
				    .attr("class", "x axis")
				    .attr("transform", "translate(0," + height + ")")
				    .call(x.axis = d3.svg.axis().scale(x).orient("bottom"));

				    svg.append("g")
				      .attr("class", "y axis")
				      .call(yAxis)
				    .append("text")
				      .attr("transform", "rotate(-90)")
				      .attr("y", 6)
				      .attr("dy", ".71em")
				      .style("text-anchor", "end")
				      .text("CPU Usage (%)");

				var path = svg.append("g")
				    .attr("clip-path", "url(#clip)")
				  .append("path")
				    .data([data])
				    .attr("class", "line");

				tick();

				function tick() {

				  // update the domains
				  now = new Date();
				  x.domain([now - (n - 2) * duration, now - duration]);
				  
				  // push the lastValue
				  data.push(latestVal);
				  
				  count = 0;

				  // redraw the line
				  svg.select(".line")
				      .attr("d", line)
				      .attr("transform", null);

				  // slide the x-axis left
				  axis.transition()
				      .duration(duration)
				      .ease("linear")
				      .call(x.axis);

				  // slide the line left
				  path.transition()
				      .duration(duration)
				      .ease("linear")
				      .attr("transform", "translate(" + x(now - (n - 1) * duration) + ")")
				      .each("end", tick);


				  // pop the old data point off the front
				  data.shift();

	
				}
				scope.$watch('points', function(newVal, oldVal){
						
						//Store the latest value temporarily so that each tick can push the latest 
						latestVal = typeof newVal === "undefined" ? 0 : newVal.usage;
						console.log(latestVal);
				}, true);


			}}}]
			);
				
	